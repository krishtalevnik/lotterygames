<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api'], function () use ($router) {

    $router->group(['prefix' => '/users'], function () use ($router) {
        $router->post('/register', 'AuthController@register');
        $router->post('/login', 'AuthController@login');
        $router->get('/user', 'AuthController@user');

        $router->group(['middleware' => "admin"], function () use ($router) {
            $router->get('/', 'UserController@getUsers');
        });

        $router->group(['middleware' => "owner"], function () use ($router) {
            $router->put('/{id:[0-9]+}', 'UserController@updateUser');
            $router->delete('/{id:[0-9]+}', 'UserController@deleteUser');
        });
    });

    $router->group(['prefix' => '/lottery_games'], function () use ($router) {
        $router->get('/', 'LotteryGameController@getGames');
    });

    $router->group(['prefix' => '/lottery_game_matches'], function () use ($router) {
        $router->group(['middleware' => "admin"], function () use ($router) {
            $router->post('/', 'LotteryGameMatchController@createGame');
            $router->put('/', 'LotteryGameMatchController@finishGame');
        });
        $router->get('/', 'LotteryGameMatchController@getMatches');
    });

    $router->group(['prefix' => '/lottery_game_match_users', 'middleware' => 'authed'], function () use ($router) {
        $router->post('/', 'LotteryGameMatchController@signForMatch');
    });
});


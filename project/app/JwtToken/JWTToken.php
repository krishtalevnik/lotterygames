<?php
declare(strict_types=1);

namespace App\JwtToken;

use App\Exceptions\ErrorExceptions\JwtTokenException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Carbon;
use JetBrains\PhpStorm\ArrayShape;
use Throwable;

class JWTToken
{
    private string $jwt;
    private string $algorithm;
    private string $key;

    public function __construct(string $key, string $algorithm)
    {
        $this->algorithm = $algorithm;
        $this->key = $key;
    }

    public function setToken(string $jwt): void
    {
        $this->jwt = $jwt;
    }

    /**
     * Creates token with sub equals id and defined expires at time.
     *
     * @param int $id
     * @param int $expireMinutes
     * @return string
     */
    public function createToken(int $id, int $expireMinutes = 0): string
    {
        $payload = $this->setPayload($id, $expireMinutes);
        $this->jwt = JWT::encode($payload, $this->key, $this->algorithm);
        return $this->jwt;
    }

    /**
     * Returns payload of current token.
     *
     * @return array
     */
    public function getPayload(): array
    {
        $payload = JWT::decode($this->jwt, new Key($this->key, $this->algorithm));
        return json_decode(json_encode($payload), true);
    }

    /**
     * Creates payload for jwt based on id and expireMinutes.
     *
     * @param int $id
     * @param int $expireMinutes
     * @return array
     */
    #[ArrayShape(['sub' => "int", 'iat' => "float|int|string", 'exp' => "float|int|string"])] private function setPayload(int $id, int $expireMinutes = 0): array
    {
        $iat = Carbon::now();
        $exp = $expireMinutes ? Carbon::now() : null;

        $payload = [
            'sub' => $id,
            'iat' => $iat->timestamp,
        ];

        if ($exp) {
            $exp->addMinutes($expireMinutes);
            $payload['exp'] = $exp->timestamp;
        }

        return $payload;
    }

    /**
     * Checks if jwt token is valid and handles JWT parser errors.
     *
     * @return bool
     * @throws JwtTokenException
     */
    public function check(): bool
    {
        try {
            $payload = $this->getPayload();
        }
        catch (Throwable $exception) {
            $message = __('jwt.failed');
            if ($exception instanceof ExpiredException) {
                $message = __('jwt.token_expired');
            }
            throw new JwtTokenException([$message]);
        }

        return true;
    }

}

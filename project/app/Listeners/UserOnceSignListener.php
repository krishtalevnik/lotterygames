<?php

namespace App\Listeners;

use App\Events\FinishMatchEvent;
use App\Events\SignForMatchEvent;
use App\Exceptions\ErrorExceptions\SignException;
use App\Models\LotteryGame;
use App\Models\LotteryGameMatch;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserOnceSignListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SignForMatchEvent $event
     * @return void
     */
    public function handle(SignForMatchEvent $event)
    {
        $match = LotteryGameMatch::find($event->match_id);
        $current_gamers = $match->users()->get();
        $data = array_values($current_gamers->toArray());
        $users = array_column($data,'pivot');
        $users = array_column($users,'user_id');

        if (in_array($event->user_id, $users)) {
            throw new SignException([__('rights.failed')]);
        }
    }
}

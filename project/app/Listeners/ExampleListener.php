<?php

namespace App\Listeners;

use App\Events\FinishMatchEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ExampleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FinishMatchEvent  $event
     * @return void
     */
    public function handle(FinishMatchEvent $event)
    {
        //
    }
}

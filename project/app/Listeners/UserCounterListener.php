<?php

namespace App\Listeners;

use App\Events\FinishMatchEvent;
use App\Events\SignForMatchEvent;
use App\Exceptions\ErrorExceptions\SignException;
use App\Models\LotteryGame;
use App\Models\LotteryGameMatch;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserCounterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SignForMatchEvent $event
     * @return void
     */
    public function handle(SignForMatchEvent $event)
    {
        $match = LotteryGameMatch::find($event->match_id);
        $current_gamers = $match->users()->count();

        $game = LotteryGame::find($match->game_id);
        $max = $game->gamer_count;

        if ($current_gamers + 1 > $max) {
            throw new SignException(__('rights.failed'));
        }
    }
}

<?php

namespace App\Listeners;

use App\Events\FinishMatchEvent;
use App\Exceptions\ErrorExceptions\SignException;
use App\Models\LotteryGame;
use App\Models\LotteryGameMatch;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SetPointsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FinishMatchEvent  $event
     * @return void
     */
    public function handle(FinishMatchEvent $event)
    {
        $match = LotteryGameMatch::find($event->match_id);
        $winnerId = $match->winner_id;
        $winner = User::filter(['id','=',$winnerId])->first();

        $gameId = $match->game_id;
        $game = LotteryGame::filter(['id','=',$gameId])->first();
        $points = $game->reward_points;

        $pointsSum = $winner->points + $points;
        $winner->update(['points'=>$pointsSum]);
    }
}

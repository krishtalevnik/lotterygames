<?php

namespace App\Listeners;

use App\Events\FinishMatchEvent;
use App\Exceptions\ErrorExceptions\SignException;
use App\Models\LotteryGame;
use App\Models\LotteryGameMatch;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class GetWinnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FinishMatchEvent  $event
     * @return void
     */
    public function handle(FinishMatchEvent $event)
    {
        $match = LotteryGameMatch::find($event->match_id);
        $current_gamers = $match->users()->get();
        $data = array_values($current_gamers->toArray());
        $users = array_column($data,'pivot');
        $users = array_column($users,'user_id');

        $winner = $users[array_rand($users)];
        $match->update(['winner_id'=>$winner]);
    }
}

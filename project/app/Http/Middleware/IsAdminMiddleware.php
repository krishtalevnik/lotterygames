<?php

namespace App\Http\Middleware;

use App\Exceptions\ErrorExceptions\AuthException;
use App\Exceptions\ErrorExceptions\SignException;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::guard('jwt_guard')->guest()) {
            throw new AuthException([__('auth.notAllowed')]);
        }
        if (!Auth::guard('jwt_guard')->user()['is_admin']) {
            throw new SignException([__('rights.notAllowed')]);
        }

        return $next($request);
    }
}

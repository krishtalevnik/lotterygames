<?php

namespace App\Http\Middleware;

use App\Exceptions\ErrorExceptions\AuthException;
use App\Exceptions\ErrorExceptions\SignException;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAuthedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('jwt_guard')->guest()) {
            throw new AuthException([__('auth.notAllowed')]);
        }

        return $next($request);
    }
}

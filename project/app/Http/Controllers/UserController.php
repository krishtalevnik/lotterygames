<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Responses\SuccessResponse;
use App\Services\Interfaces\IUserServiceInterface;

class UserController extends Controller
{
    protected IUserServiceInterface $userService;

    public function __construct(IUserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    public function getUsers()
    {
        $data = $this->userService->repository->getAll()->toArray();
        return SuccessResponse::response($data, __('successResponses.success'), 200);
    }

    public function deleteUser(int $id): \Illuminate\Http\JsonResponse
    {
        $data = $this->userService->deleteUser($id);
        return SuccessResponse::response(null, __('successResponses.success'), 200);
    }

    public function updateUser(int $id, UpdateUserRequest $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();
        $data = $this->userService->updateUser($id, $validated);
        return SuccessResponse::response($data, __('successResponses.success'), 200);
    }
}


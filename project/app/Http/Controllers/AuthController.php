<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Responses\SuccessResponse;
use App\Services\Interfaces\IAuthServiceInterface;

class AuthController extends Controller
{
    protected IAuthServiceInterface $authService;

    public function __construct(IAuthServiceInterface $authService)
    {
        $this->authService = $authService;
    }

    public function register(RegisterRequest $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();
        $user = $this->authService->register($validated)->toArray();
        return SuccessResponse::response($user, __('successResponses.success'), 201);
    }

    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();
        $data = $this->authService->login($validated);
        return SuccessResponse::response($data, __('successResponses.success'), 200);
    }

    public function user(): \Illuminate\Http\JsonResponse
    {
        $user = $this->authService->getUser();
        $user = $user?->toArray();
        return SuccessResponse::response($user, __('successResponses.success'), 200);
    }
}


<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLotteryGameMatchRequest;
use App\Http\Requests\GetLotteryGameMatchesRequest;
use App\Http\Requests\UpdateLotteryGameMatchRequest;
use App\Responses\SuccessResponse;
use App\Services\LotteryGameService;

class LotteryGameController extends Controller
{
    protected LotteryGameService $lotteryGameService;

    public function __construct()
    {
        $this->lotteryGameService = new LotteryGameService();
    }

    public function getGames(): \Illuminate\Http\JsonResponse
    {
        $data = $this->lotteryGameService->repository->getAll()->toArray();
        return SuccessResponse::response($data, __('successResponses.success'), 200);
    }
}


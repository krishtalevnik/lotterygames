<?php

namespace App\Http\Controllers;

use App\Events\FinishMatchEvent;
use App\Events\SignForMatchEvent;
use App\Http\Requests\CreateLotteryGameMatchRequest;
use App\Http\Requests\GetLotteryGameMatchesRequest;
use App\Http\Requests\UpdateLotteryGameMatchRequest;
use App\Responses\SuccessResponse;
use App\Services\Interfaces\IAuthServiceInterface;
use App\Services\Interfaces\ILotteryGameMatchServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class LotteryGameMatchController extends Controller
{
    protected ILotteryGameMatchServiceInterface $lotteryGameMatchService;
    protected IAuthServiceInterface $authService;

    public function __construct(ILotteryGameMatchServiceInterface $lotteryGameMatchService, IAuthServiceInterface $authService)
    {
        $this->lotteryGameMatchService = $lotteryGameMatchService;
        $this->authService = $authService;
    }

    public function createGame(CreateLotteryGameMatchRequest $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();
        $data = $this->lotteryGameMatchService->repository->create($validated)->toArray();
        return SuccessResponse::response($data, __('successResponses.success'), 200);
    }

    public function finishGame(UpdateLotteryGameMatchRequest $request): \Illuminate\Http\JsonResponse
    {
        $id = $request->validated()['id'];
        $data = $this->lotteryGameMatchService->finishGame($id);
        Event::dispatch(new FinishMatchEvent($id));
        return SuccessResponse::response(null, __('successResponses.success'), 200);
    }

    public function getMatches(GetLotteryGameMatchesRequest $request): \Illuminate\Http\JsonResponse
    {
        $id = $request->validated()['lottery_game_id'];
        $data = $this->lotteryGameMatchService->repository->getMatchesByGameID($id);
        $data = $data?->toArray();
        return SuccessResponse::response($data, __('successResponses.success'), 200);
    }

    public function signForMatch(UpdateLotteryGameMatchRequest $request): \Illuminate\Http\JsonResponse
    {
        $id = $request->validated()['id'];
        $user_id = $this->authService->getUser()['id'];
        Event::dispatch(new SignForMatchEvent($id, $user_id));

        $match = $this->lotteryGameMatchService->signForMatch($id, $user_id)->toArray();
        return SuccessResponse::response($match, __('successResponses.success'), 200);
    }
}


<?php

namespace App\Http\Requests;

use Anik\Form\FormRequest;

class UpdateUserRequest extends FormRequest
{

    protected function rules(): array
    {
        return [
            'first_name' => 'string',
            'last_name' => 'string',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Anik\Form\FormRequest;

class CreateLotteryGameMatchRequest extends FormRequest
{

    protected function rules(): array
    {
        return [
            'game_id'=>'required|exists:lottery_games,id',
            'start_date'=>'required|date|after_or_equal:now',
            'start_time'=>'required|date_format:H:i',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Anik\Form\FormRequest;

class RegisterRequest extends FormRequest
{

    protected function rules(): array
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
        ];
    }
}

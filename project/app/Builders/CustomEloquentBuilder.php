<?php
declare(strict_types=1);

namespace App\Builders;

use Illuminate\Database\Eloquent\Builder as BaseBuilder;

class CustomEloquentBuilder extends BaseBuilder
{
    public function filter(array $filter): CustomEloquentBuilder
    {
        if (!is_array($filter[0])) {
            $this->where($filter[0], $filter[1], $filter[2]);
        }
        else {
            $this->where($filter);
        }

        return $this;
    }

    public function withs(array $withs): BaseBuilder
    {
        return $this->with($withs);
    }

    public function find($id, $columns = ['*'])
    {
        return parent::find(['id' => $id], $columns)->first();
    }

}

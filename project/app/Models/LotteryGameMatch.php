<?php

namespace App\Models;

class LotteryGameMatch extends BaseModel
{
    protected $fillable = [
        'game_id',
        'start_date',
        'start_time',
        'is_finished',
        'winner_id',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'lottery_game_match_users');
    }
}

<?php

namespace App\Models;

use App\Builders\CustomEloquentBuilder;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    public function newEloquentBuilder($query): CustomEloquentBuilder
    {
        return new CustomEloquentBuilder($query);
    }
}

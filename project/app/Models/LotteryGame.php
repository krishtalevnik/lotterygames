<?php

namespace App\Models;

class LotteryGame extends BaseModel
{
    protected $fillable = [
        'name',
        'gamer_count',
        'reward_points'
    ];

    public function matches(){
         return $this->hasMany(LotteryGameMatch::class,'game_id')->orderBy('start_time','desc')->orderBy('start_date','desc');
    }
}

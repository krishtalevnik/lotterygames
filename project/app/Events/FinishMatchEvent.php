<?php

namespace App\Events;

class FinishMatchEvent extends Event
{
    public $match_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($match_id)
    {
        $this->match_id = $match_id;
    }
}

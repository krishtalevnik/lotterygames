<?php

namespace App\Events;

class SignForMatchEvent extends Event
{
    public $match_id;
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($match_id, $user_id)
    {
        $this->match_id = $match_id;
        $this->user_id= $user_id;
    }
}

<?php

namespace App\Providers;

use App\Services\Interfaces\ILotteryGameMatchServiceInterface;
use App\Services\Interfaces\IUserServiceInterface;
use App\Services\LotteryGameMatchService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IUserServiceInterface::class, UserService::class);
        $this->app->singleton(ILotteryGameMatchServiceInterface::class, LotteryGameMatchService::class);
    }
}

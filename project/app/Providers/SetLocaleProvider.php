<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class SetLocaleProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Sets language for responses.
     *
     * @return void
     */
    public function boot()
    {
        $locale = request()->header('locale') ?? 'en';
        app('translator')->setLocale($locale);
    }
}

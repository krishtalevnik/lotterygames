<?php

namespace App\Providers;

use App\Guards\JwtGuard;
use App\Services\Interfaces\IAuthServiceInterface;
use App\Services\JWTAuthService;
use App\JwtToken\JWTToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Services
        $this->app->singleton(IAuthServiceInterface::class, JWTAuthService::class);

        // Providers

        // Drivers

        Auth::extend('jwt_guard', function ($app, $name, array $config) {
            $jwtToken = new JWTToken( env('JWT_SECRET'), 'HS256');
            $request = $app->request;
            return new JwtGuard(Auth::createUserProvider($config['provider']), $request, $jwtToken);
        });
    }
}

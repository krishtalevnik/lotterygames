<?php

namespace App\Providers;

use App\Events\FinishMatchEvent;
use App\Events\SignForMatchEvent;
use App\Listeners\GetWinnerListener;
use App\Listeners\SetPointsListener;
use App\Listeners\UserCounterListener;
use App\Listeners\UserOnceSignListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        SignForMatchEvent::class => [
            UserCounterListener::class,
            UserOnceSignListener::class
        ],
        FinishMatchEvent::class => [
            GetWinnerListener::class,
            SetPointsListener::class,
        ],
    ];
}

<?php

namespace App\Services;

use App\Exceptions\ErrorExceptions\AuthException;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\Interfaces\IAuthServiceInterface;
use Illuminate\Support\Facades\Auth;

class JWTAuthService implements IAuthServiceInterface
{
    public UserRepository $repository;

    public function __construct()
    {
        $this->repository = new UserRepository();
    }

    public function register(array $params): User
    {
        $params['password'] = app('hash')->make($params['password']);
        $user = $this->repository->create($params);
        return $user;
    }

    public function login(array $credentials): array
    {
        if (! $token = Auth::guard('jwt_guard')->attempt($credentials)) {
            throw new AuthException([__('auth.failed')]);
        }
        return ['auth_token' => $token];
    }

    public function getUser(): ?\Illuminate\Contracts\Auth\Authenticatable
    {
        return Auth::guard('jwt_guard')->user();
    }
}

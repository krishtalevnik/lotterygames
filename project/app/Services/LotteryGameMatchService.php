<?php

namespace App\Services;

use App\Models\LotteryGameMatch;
use App\Repositories\LotteryGameMatchRepository;
use App\Services\Interfaces\ILotteryGameMatchServiceInterface;

class LotteryGameMatchService implements ILotteryGameMatchServiceInterface
{
    public LotteryGameMatchRepository $repository;

    public function __construct()
    {
        $this->repository = new LotteryGameMatchRepository();
    }


    public function finishGame(int $id): LotteryGameMatch
    {
        $game = LotteryGameMatch::find($id);
        $game->update(['is_finished' => true]);
        return $game;
    }

    public function signForMatch(int $id, int $user_id)
    {
        $game = LotteryGameMatch::where('id','=',$id)->first();
        $game->users()->attach($user_id);
        $game = LotteryGameMatch::where('id','=',$id)->with('users')->first();
        return $game;
    }
}

<?php

namespace App\Services\Interfaces;

use App\Models\User;

interface IAuthServiceInterface
{
    public function getUser(): ?\Illuminate\Contracts\Auth\Authenticatable;

    public function login(array $credentials): array;

    public function register(array $params): User;
}

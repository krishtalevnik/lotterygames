<?php

namespace App\Services\Interfaces;

use App\Models\LotteryGameMatch;

interface ILotteryGameMatchServiceInterface
{
    public function finishGame(int $id): LotteryGameMatch;

    public function signForMatch(int $id, int $user_id);
}

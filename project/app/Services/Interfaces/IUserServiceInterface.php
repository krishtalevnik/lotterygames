<?php

namespace App\Services\Interfaces;

interface IUserServiceInterface
{
    public function deleteUser(int $id);

    public function updateUser(int $id, array $params);
}

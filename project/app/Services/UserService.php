<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\Interfaces\IUserServiceInterface;

class UserService implements IUserServiceInterface
{
    public UserRepository $repository;

    public function __construct()
    {
        $this->repository = new UserRepository();
    }


    public function deleteUser(int $id)
    {
        User::destroy($id);
    }

    public function updateUser(int $id, array $params)
    {
        $user = User::find($id);
        $user->update($params);
    }
}

<?php

namespace App\Services;

use App\Repositories\LotteryGameRepository;

class LotteryGameService
{
    public LotteryGameRepository $repository;

    public function __construct()
    {
        $this->repository = new LotteryGameRepository();
    }

}

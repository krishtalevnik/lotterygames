<?php

namespace App\Repositories\Interfaces;

interface IBaseInterface
{
    public function setModel();

    public function getOne($id);

    public function getAll();

    public function create(array $params);
}

<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = $this->setModel();
    }

    public function setModel()
    {
        return new User();
    }

    public function getAll()
    {
        $users = User::all();

        foreach ($users as $user) {
           $winnedMatches = $user->winnedMatches()->get()->toArray();
           $user['winned_matches'] = $winnedMatches;
        }

        return $users;
    }
}

<?php

namespace App\Repositories;

use App\Models\LotteryGame;

class LotteryGameRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = $this->setModel();
    }

    public function setModel()
    {
        return new LotteryGame();
    }

    public function getAll()
    {
        return LotteryGame::with('matches')->get();
    }
}

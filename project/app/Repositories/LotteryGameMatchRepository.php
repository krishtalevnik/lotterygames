<?php

namespace App\Repositories;

use App\Models\LotteryGameMatch;

class LotteryGameMatchRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = $this->setModel();
    }

    public function setModel()
    {
        return new LotteryGameMatch();
    }

    public function getMatchesByGameID(int $id)
    {
        $matches = LotteryGameMatch::find($id);
        return $matches;
    }
}

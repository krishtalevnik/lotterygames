<?php

namespace App\Repositories;

use App\Repositories\Interfaces\IBaseInterface;

abstract class BaseRepository implements IBaseInterface
{
    public $model;

    public function getModel()
    {
        return $this->model;
    }

    public function getOne($id)
    {
        return $this->model->find($id);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function create(array $params)
    {
        return $this->model::create($params);
    }

    public function where($params)
    {
        return $this->model::where($params);
    }
}

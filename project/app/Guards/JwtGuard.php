<?php

namespace App\Guards;

use App\JwtToken\JWTToken;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class JwtGuard implements Guard
{
    private JWTToken $jwtTokenService;
    private UserProvider $userProvider;
    private Request $request;
    private int $expireTime;

    public function __construct(UserProvider $createUserProvider, Request $request, JWTToken $jwtTokenService)
    {
        $this->jwtTokenService = $jwtTokenService;
        $this->userProvider = $createUserProvider;
        $this->request = $request;
        $this->expireTime = 0;
    }

    /**
     * Attempt to authenticate the user using the given credentials and return the token or false.
     *
     * @param array $credentials
     * @param bool $login
     * @return bool|string
     */
    public function attempt(array $credentials = [], bool $login = true): bool|string
    {
        $user = $this->userProvider->retrieveByCredentials($credentials);

        if ($this->hasValidCredentials($user, $credentials)) {
            return $login ? $this->login($user) : true;
        }

        return false;
    }

    /**
     * Determine if the user matches the credentials.
     *
     * @param mixed $user
     * @param array $credentials
     * @return bool
     */
    protected function hasValidCredentials($user, array $credentials): bool
    {
        return $user !== null && $this->userProvider->validateCredentials($user, $credentials);
    }

    /**
     * Sets expire time for created by guard tokens.
     *
     * @param $expireTime
     */
    public function setExpireTime($expireTime)
    {
        $this->expireTime = $expireTime;
    }

    public function login($user): string
    {
        return $this->jwtTokenService->createToken($user->id, $this->expireTime);
    }

    /**
     * Checks if user is authenticated.
     *
     * @return bool
     */
    public function check(): bool
    {
        return !is_null($this->user());
    }

    /**
     * Checks if user is not authenticated.
     *
     * @return bool
     */
    public function guest(): bool
    {
        return !$this->check();
    }

    /**
     * Gets user from provided jwt.
     *
     * @return Authenticatable|null
     */
    public function user(): ?Authenticatable
    {
        return $this->user = $this->userProvider->retrieveById($this->id());
    }

    /**
     * Gets id from provided jwt token.
     *
     * @return mixed
     */
    public function id(): mixed
    {
        $token = $this->request->headers->get('Authorization');
        if (!$token) return null;
        $this->jwtTokenService->setToken($token);
        if ($this->jwtTokenService->check()) {
            return $this->jwtTokenService->getPayload()['sub'];
        }

        return null;
    }

    /**
     * Checks user credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = []): bool
    {
        return (bool)$this->attempt($credentials, false);
    }

    public function setUser(Authenticatable $user)
    {

    }
}

<?php

namespace App\Exceptions\ErrorExceptions;

use App\Responses\ErrorResponse;
use Exception;

abstract class BaseException extends Exception
{
    protected ?array $errors;
    protected int $status;

    public function __construct(?array $errors, int $status = 422, Exception $previous = NULL)
    {
        parent::__construct("BaseErrorHandling", $status, $previous);
        $this->status = $status;
        $this->errors = $errors;
    }

    public function render(): \Illuminate\Http\JsonResponse
    {
        return ErrorResponse::response($this->errors, __('exceptions.notFound.user'), $this->status);
    }
}

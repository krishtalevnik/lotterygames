<?php

namespace App\Exceptions\ErrorExceptions;

use App\Responses\ErrorResponse;

class AuthException extends BaseException
{
    public function render(): \Illuminate\Http\JsonResponse
    {
        return ErrorResponse::response($this->errors, __('auth.failed'),401);
    }
}

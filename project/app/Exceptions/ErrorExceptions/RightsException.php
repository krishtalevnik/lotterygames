<?php

namespace App\Exceptions\ErrorExceptions;

use App\Responses\ErrorResponse;

class RightsException extends BaseException
{
    public function render(): \Illuminate\Http\JsonResponse
    {
        return ErrorResponse::response($this->errors, __('rights.failed'),403);
    }
}

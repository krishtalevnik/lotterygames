<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{

    public function run(){
        $this->hardcodeAdmin();
        User::factory()
            ->count(2)
            ->create();
    }

    private function hardcodeAdmin() {
        if (!User::where('email', '=', 'admin@gmail.com')->exists()) {
            DB::table('users')->insert([
                'first_name' => Str::random(10),
                'last_name' => Str::random(10),
                'email' => 'admin@gmail.com',
                'password' => Hash::make('password'),
                'is_admin' => true
            ]);
        }
    }
}

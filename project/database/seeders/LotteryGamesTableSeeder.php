<?php

namespace Database\Seeders;

use App\Models\LotteryGame;
use Illuminate\Database\Seeder;

class LotteryGamesTableSeeder extends Seeder
{
    private function getLotteryGames()
    {
        return [
            [
                'name' => 'GuessNumber',
                'gamer_count' => 2,
                'reward_points' => 100
            ],
            [
                'name' => 'LoseOrFail',
                'gamer_count' => 5,
                'reward_points' => 200
            ],
            [
                'name' => 'Lucky7',
                'gamer_count' => 7,
                'reward_points' => 300
            ]
        ];
    }

    public function run()
    {
        $games = $this->getLotteryGames();

        foreach ($games as $game) {
            if (!LotteryGame::where('name', '=', $game['name'])->exists()) LotteryGame::create($game);
        }

    }
}
